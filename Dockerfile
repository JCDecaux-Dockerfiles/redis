FROM jcdecaux/rhel:7
MAINTAINER Nicolas Mallet <nicolas.mallet@jcdecaux.com>

RUN yum install supervisor redis -y \
  && yum clean all

COPY redis.conf /etc/redis.conf
COPY supervisord.conf /etc/supervisord.conf

VOLUME ["/var/lib/redis"]

EXPOSE 6379

CMD ["/usr/bin/supervisord"]
